(function() {
    'use strict';

    angular
        .module('jhbitbucketApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
