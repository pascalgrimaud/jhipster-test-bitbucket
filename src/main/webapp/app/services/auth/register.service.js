(function () {
    'use strict';

    angular
        .module('jhbitbucketApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
